package com.maxbill.base.bean;

import lombok.Data;

@Data
public class InfoNode {

    private String key;

    private String val;

    private String txt;

}
