package com.maxbill.base.service;

/**
 * 数据业务层接口
 *
 * @author MaxBill
 * @date 2019/07/06
 */
public interface DataService {

    /**
     * 初始化系统数据
     */
    void initSystemData();

}
