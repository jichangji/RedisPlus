package com.maxbill.core.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 数据源配置
 *
 * @author MaxBill
 * @date 2019/07/06
 */
@Configuration
public class DataSourceConfig {

    private String baseUrl = System.getProperty("user.home");

    @Bean
    public DataSource dataSource() {
        HikariConfig datasource = new HikariConfig();
        datasource.setPoolName(getClass().getName());
        datasource.setJdbcUrl("jdbc:derby:" + baseUrl + "/.RedisPlus/data;create=true");
        datasource.setMaximumPoolSize(10);
        datasource.setConnectionTimeout(5000);
        datasource.setIdleTimeout(3000);
        return new HikariDataSource(datasource);
    }

}
